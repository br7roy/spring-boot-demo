 /*
  * Package com.rust.springbootdemo
  * FileName: TestCls
  * Author:   Takho
  * Date:     19/1/8 23:01
  */
 package com.rust.springbootdemo;

 import junit.framework.TestCase;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

 /**
  * @author Takho
  */
 public class TestCls extends TestCase {
  public static final String URL = "http://localhost:8080";


  public void testHttpRequest() throws Exception {

   URL url = new URL(URL + "/order?orderNo=123");

   HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
   urlConnection.setRequestMethod("POST");
   urlConnection.setConnectTimeout(5000);
   urlConnection.setDoOutput(true);
   urlConnection.connect();


   InputStream inputStream = urlConnection.getInputStream();
   StringBuilder stringBuilder = new StringBuilder();

   //字符
/*   char[] buf = new char[1];
   int n = 0;
   BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
   for (;(n=bufferedReader.read(buf, 0, buf.length))!=-1;) {
       stringBuilder.append(new java.lang.String(buf, 0, n));
       if (n != buf.length) {
           break;
       }
   }
   System.out.println(stringBuilder.toString());*/

   //字节
/*   byte[] bytes = new byte[1024];
   int ret = inputStream.read(bytes, 0, bytes.length);
   ByteArrayOutputStream fbos = new ByteArrayOutputStream();
   while (ret != -1) {
       fbos.write(bytes, 0, bytes.length);
       ret = inputStream.read(bytes, 0, bytes.length);
       stringBuilder.append(fbos);
   }
   System.out.println(stringBuilder.toString());*/



 /*  BufferedInputStream bis = new BufferedInputStream(inputStream);

//   StringBuilder stringBuilder = new StringBuilder();
   int len = 0;
   while ((len = bis.read()) > 0) {
    stringBuilder.append(len);
   }
   System.out.println(stringBuilder.toString());*/
  }
 }
