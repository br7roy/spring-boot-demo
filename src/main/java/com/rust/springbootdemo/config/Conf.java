 /*
  * Package com.rust.springbootdemo.config
  * FileName: DBConf
  * Author:   Rust
  * Date:     2018/10/30 20:29
  */
 package com.rust.springbootdemo.config;

 import org.apache.catalina.Context;
 import org.apache.tomcat.util.http.LegacyCookieProcessor;
 import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
 import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
 import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
 import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;

 /**
  * @author Rust
  */
 @Configuration
 public class Conf {
	 @Bean
	 EmbeddedServletContainerCustomizer processor() {
		 return new EmbeddedServletContainerCustomizer() {
			 @Override
			 public void customize(ConfigurableEmbeddedServletContainer factory) {
				 if (factory instanceof TomcatEmbeddedServletContainerFactory) {
					 TomcatEmbeddedServletContainerFactory tomcatFactory =
							 (TomcatEmbeddedServletContainerFactory) factory;
					 tomcatFactory.addContextCustomizers(new TomcatContextCustomizer() {
						 @Override
						 public void customize(Context context) {
							 context.setCookieProcessor(new LegacyCookieProcessor());
						 }
					 });
				 }
			 }
		 };
	 }

/*	 @Bean
	 public WebMvcConfigurer corsConfigurer() {
		 return new WebMvcConfigurerAdapter() {
			 @Override
			 public void addCorsMappings(CorsRegistry registry) {
				 registry.addMapping("/first/**").allowedOrigins("localhost");
			 }
		 };
	 }*/
/*	 @Bean
	 public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer() {
		 return new WebServerFactoryCustomizer<ConfigurableWebServerFactory>() {
			 @Override
			 public void customize(ConfigurableWebServerFactory factory) {
				 factory.setPort(8081);
			 }
		 };
	 }*/

/*	 @Component
	 static class InheritCookieConf implements EmbeddedServletContainerCustomizer {

		 @Override
		 public void customize(ConfigurableEmbeddedServletContainer configurableEmbeddedServletContainer) {
			 TomcatEmbeddedServletContainerFactory factory = (TomcatEmbeddedServletContainerFactory) configurableEmbeddedServletContainer;
			 factory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
				 @Override
				 public void customize(Connector connector) {
*//*    Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
    protocol.setMaxConnections(200);
    protocol.setMaxThreads(200);
    protocol.setSelectorTimeout(3000);
    protocol.setSessionTimeout(3000);
    protocol.setConnectionTimeout(3000);*//*
					 connector.setDomain(".hello.com");
				 }
			 });
		 }
	 }*/
/*  @Bean
  public TomcatEmbeddedServletContainerFactory customizer() {
  }*/


 }