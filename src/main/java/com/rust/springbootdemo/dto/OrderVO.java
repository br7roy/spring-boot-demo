 /*
  * Package com.rust.springbootdemo.dto
  * FileName: OrderVO
  * Author:   Takho
  * Date:     19/1/8 22:57
  */
 package com.rust.springbootdemo.dto;

 /**
  * @author Takho
  */
 public class OrderVO extends CommonVO{

  private String orderNo;
  private String orderStatus;

  public String getOrderNo() {
   return orderNo;
  }

  public void setOrderNo(String orderNo) {
   this.orderNo = orderNo;
  }

  public String getOrderStatus() {
   return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
   this.orderStatus = orderStatus;
  }
 }
