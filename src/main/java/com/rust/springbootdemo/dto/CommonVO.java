 /*
  * Package com.rust.springbootdemo.dto
  * FileName: CommonVO
  * Author:   Takho
  * Date:     19/1/8 22:56
  */
 package com.rust.springbootdemo.dto;

 /**
  * @author Takho
  */
 public abstract class CommonVO implements IVO {
	 private String code;
	 private String msg;

	 public String getCode() {
		 return code;
	 }

	 public void setCode(String code) {
		 this.code = code;
	 }

	 public String getMsg() {
		 return msg;
	 }

	 public void setMsg(String msg) {
		 this.msg = msg;
	 }

 }
