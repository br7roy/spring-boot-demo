/*
 * Package com.rust.springbootdemo.controller
 * FileName: DemoController
 * Author:   Administrator
 * Date:     2017/12/1 21:55
 */
package com.rust.springbootdemo.controller;

import com.rust.springbootdemo.dto.CommonVO;
import com.rust.springbootdemo.dto.OrderVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * FileName:    DemoController
 * Author:      Administrator
 * Date:        2017/12/1
 * Description:
 */
@Controller
public class DemoController extends BaseController {


	@RequestMapping("/first")
	public String main(HttpServletRequest request, HttpServletResponse response) {

		for (Cookie cookie : request.getCookies()) {
			cookie.setDomain(".baidu.com");
			System.out.println(cookie.getName() + cookie.getValue());
			System.out.println(cookie.getDomain());
			response.addCookie(cookie);
		}

		return "index";

	}


	@RequestMapping(path = "order", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json; charset=UTF-8")
	@ResponseBody
	public CommonVO doOrder(HttpServletRequest request, @RequestParam String orderNo) {
		OrderVO vo = new OrderVO();
		vo.setOrderNo("123123");
		vo.setOrderStatus("00");
		return vo;
	}

}
