/*
 * Package com.rust.springbootdemo.controller
 * FileName: BaseController
 * Author:   Administrator
 * Date:     2017/12/1 21:56
 */
package com.rust.springbootdemo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * FileName:    BaseController
 * Author:      Administrator
 * Date:        2017/12/1
 * Description:
 */
@CrossOrigin
		(origins="localhost",maxAge=3600)
public class BaseController {
}
